package com.poema.lib;

import com.backendless.BackendlessUser;
import com.poema.entities.FriendsRelate;

/**
 * Created by alexander on 31.10.2016.
 */
public class DataLib {

    public static BackendlessUser getAnotherFriendFromRelate(FriendsRelate input, String myUserId){
        if(input.UserOne.getObjectId().equals(myUserId))
            return input.UserTwo;
        else if(input.UserTwo.getObjectId().equals(myUserId))
            return input.UserOne;
        else  return null;
    };

}
