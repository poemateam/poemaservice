package com.poema.lib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Formatter;

/**
 * Created by alexander on 25.08.2016.
 */
public class Encrypt {

    public static byte[] decodeBase64(String base64)
    {
        return Base64.getDecoder().decode(base64);
    }

    public static String getHash(byte[] array)
    {
        return SHAsum(array);
    }

    public static String getAlternativeSubHash(byte[] array)
    {
        byte[] help = new byte[array.length];
        for(int i =0; i < array.length; i++)
            help[i] = (byte) (((int)array[i] * 31)%255);

        return SHAsum(help).substring(0,2);
    }


    private static String SHAsum(byte[] convertme)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byteArray2Hex(md.digest(convertme));
    }

    private static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

}
