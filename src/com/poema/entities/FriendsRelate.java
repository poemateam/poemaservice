package com.poema.entities;

import com.backendless.BackendlessUser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 31.10.2016.
 */
public class FriendsRelate {

    public String objectId;
    public Integer Status;
    public BackendlessUser UserOne;
    public BackendlessUser UserTwo;


    public FriendsRelate(){

    };

    public FriendsRelate(Map data){

        //this.objectId = data.get("o")
        this.Status= (Integer) data.get("Status");
        this.UserOne = (BackendlessUser) data.get("UserOne");
        this.UserTwo = (BackendlessUser) data.get("UserTwo");
        this.objectId = (String) data.get("objectId");

    }
}
