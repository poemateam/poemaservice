package com.poema.events.persistense_service;


import com.backendless.Backendless;
import com.backendless.messaging.DeliveryOptions;
import com.backendless.messaging.PublishOptions;
import com.backendless.servercode.ExecutionResult;
import com.backendless.servercode.RunnerContext;
import com.backendless.servercode.annotation.Asset;
import com.backendless.services.messaging.MessageStatus;


import java.util.HashMap;
import java.util.Map;

/**
 * GenericTableEventHandler handles events for all entities. This is accomplished
 * with the @Asset( "*" ) annotation.  The asterisk denotes "all entities".
 * The methods in the class correspond to the events selected in Backendless
 * Console.
 */

@Asset( "*" )
public class GenericChannelEventHandler extends com.backendless.servercode.extension.MessagingExtender
{

    @Override
    public void afterPublish(RunnerContext context, Object message, PublishOptions publishOptions, DeliveryOptions deliveryOptions, ExecutionResult<MessageStatus> result ) throws Exception
    {
        Map<String,Object> ms = new HashMap<>();

        String poemaId = publishOptions.getHeaders().get("poemaId");
        String messageId =  publishOptions.getHeaders().get("messageId");

        if(poemaId!=null) {
            ms.put("poemaId", poemaId);
        }
        if(messageId!=null) {
            ms.put("messageId", messageId);
        }
            ms.put("sub", publishOptions.getSubtopic());
            ms.put("owner",context.getUserId());
            ms.put("message",message);
            Map lastMessage = Backendless.Persistence.of("MessageLog").save(ms);

            if(poemaId!=null){

                HashMap<String, Object> relatePoema = new HashMap<>();
                relatePoema.put("objectId",poemaId);
                relatePoema.put("___class","Poema");

                HashMap<String,Object> forLastMessage = new HashMap<>();

                forLastMessage.put("___class", "MessageLog");
                forLastMessage.put("objectId", lastMessage.get("objectId"));

                relatePoema.put("lastMessage",forLastMessage);

                Backendless.Persistence.of("Poema").save(relatePoema);

            }


    }

}