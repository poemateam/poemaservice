package com.poema.events.persistense_service;


        import com.backendless.Backendless;
        import com.backendless.servercode.ExecutionResult;
        import com.backendless.servercode.RunnerContext;
        import com.backendless.servercode.annotation.Asset;


        import java.util.HashMap;

/**
 * GenericTableEventHandler handles events for all entities. This is accomplished
 * with the @Asset( "*" ) annotation.  The asterisk denotes "all entities".
 * The methods in the class correspond to the events selected in Backendless
 * Console.
 */

@Asset( "*" )
public class GenericTableEventHandler extends com.backendless.servercode.extension.PersistenceExtender<HashMap>
{


    @Override
    public void afterCreate(RunnerContext context, HashMap hashMap, ExecutionResult<HashMap> entity) throws Exception {

        String className = (String) hashMap.get("___class");
        HashMap map = entity.getResult();
        map.put("___class", className);
        if(!className.equals("MessageLog"))
         Backendless.Data.of(className).save(map);

    }


}