package com.poema.services;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.commons.exception.BackendlessExceptionMessage;
import com.backendless.commons.exception.BackendlessServerException;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.backendless.servercode.IBackendlessService;
import com.backendless.servercode.InvocationContext;
import com.poema.entities.FriendsRelate;
import com.poema.lib.DataLib;
import com.poema.lib.Encrypt;
import com.poema.lib.Prefs;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 24.08.2016.
 */
public class PoemaUserService implements IBackendlessService {

    public Map<String,String> uploadUserAvatar(String base64Avatar, String base64SmallAvatar)
    {
         Map<String,String> answer = new HashMap<>();

         String userId = InvocationContext.getUserId();
         BackendlessUser user = Backendless.UserService.findById(userId);

         byte[] image = Encrypt.decodeBase64(base64Avatar);
         byte[] imageSmall = Encrypt.decodeBase64(base64SmallAvatar);

         String imageHash = Encrypt.getHash(image);
         String dirHash = Encrypt.getAlternativeSubHash(image);

         String imageUrlPath = Backendless.getUrl() + "/" + Backendless.getApplicationId() + "/" + Backendless.getVersion() + "/files/" + Prefs.FILE_AVATARS_PATH + "/" + dirHash + "/" + imageHash;
         String imageUrlPathSmall = Backendless.getUrl() + "/" + Backendless.getApplicationId() + "/" + Backendless.getVersion() + "/files/" + Prefs.FILE_AVATARS_PATH + "/" + dirHash + "/" + imageHash+"mini";
         String imagePath = Prefs.FILE_AVATARS_PATH + "/" + dirHash;

         if(!Backendless.Files.exists(Prefs.FILE_AVATARS_PATH+"/"+dirHash+"/"+imageHash)){

            imageUrlPath = Backendless.Files.saveFile(imagePath,imageHash,image);
            imageUrlPathSmall = Backendless.Files.saveFile(imagePath,imageHash+"mini",imageSmall);

         }

         user.setProperty("image",imageUrlPath);
         user.setProperty("small_image", imageUrlPathSmall);
         Backendless.UserService.update(user);

         answer.put("image",imageUrlPath);
         answer.put("small_image", imageUrlPathSmall);

        return answer;
    }

    public Map<String,Object> getUserWithRelate(String userID){

        Map<String,Object> answer = new HashMap<>();
        String invocationUserId = InvocationContext.getUserId();

        BackendlessDataQuery query = new BackendlessDataQuery();

        query.setWhereClause("(userOne.objectId = '" + userID + "' and userTwo.objectId = '" + invocationUserId + "') or "
                + "(userOne.objectId = '" + invocationUserId + "' and userTwo.objectId = '" + userID + "')"
        );

        BackendlessUser resultUser = Backendless.Data.of(BackendlessUser.class).findById(userID);
        BackendlessCollection resultRelation = Backendless.Data.of("FriendsRelate").find(query);

        answer.put("User",resultUser);
        answer.put("Relation",resultRelation);


        return answer;

    };


    public List<FriendsRelate> getFriends(String userId, int offset, int pageSize, int statusCode){ // < 0 - все, 0 - только мои заявки, 1 - только приглашения, 2 - только друзья, 3 - заявки + друзья
        Map<String,Object> answer = new HashMap<>();
        String invocationUserId = InvocationContext.getUserId();

        // Описание поля Status:
        // 0 - отправлен запрос
        //     отправитель и получатель UserOne и UserTwo соответственно
        // 2 - мой друг



        QueryOptions queryOptions = new QueryOptions();
        BackendlessDataQuery query = new BackendlessDataQuery();

        queryOptions.setPageSize(pageSize);
        queryOptions.setOffset(offset);
        List<String> sortBy = new ArrayList<>();
        sortBy.add("status");
        sortBy.add("updated DESC");
        sortBy.add("created DESC");
        queryOptions.setSortBy(sortBy);

        query.setQueryOptions(queryOptions);



        String additionalFlag = "status = 2";

        if(invocationUserId.equals(userId)){
            if(statusCode<0)
                additionalFlag = "status IN (0,2)";
            else if(statusCode == 0){
                additionalFlag = "status = 0 AND UserOne.objectId = '"+ invocationUserId +"'";
            } else if(statusCode == 1){
                additionalFlag = "status = 0 AND UserTwo.objectId = '"+ invocationUserId +"'";
            } else if(statusCode == 3)
                additionalFlag = "((status = 0 AND UserTwo.objectId = '"+ invocationUserId +"') OR status = 2)";
        };


        query.setWhereClause("(UserOne.objectId = '" + userId + "' or UserTwo.objectId = '" + userId + "') and " + additionalFlag);



        BackendlessCollection<Map> allFriends = Backendless.Data.of("FriendsRelate").find(query);




        List<FriendsRelate> friends = new ArrayList<>();





        String friendsInClause = "";

        //создаем список из элементов
        for(int i = 0 ; i < allFriends.getCurrentPage().size(); i++){

                String userOneId = ((BackendlessUser)allFriends.getCurrentPage().get(i).get("UserOne")).getUserId();

                BackendlessUser friend = null;

                if(!userOneId.equals(userId))
                    friend = ((BackendlessUser) allFriends.getCurrentPage().get(i).get("UserOne"));
                else friend = ((BackendlessUser) allFriends.getCurrentPage().get(i).get("UserTwo"));


                 FriendsRelate relate = new FriendsRelate(allFriends.getCurrentPage().get(i));

                 if(!userId.equals(invocationUserId)){
                     relate.Status = -1; //unknown
                 }

                  friends.add(relate);

                if(!invocationUserId.equals(userId)){

                    if(friendsInClause.length()==0)
                        friendsInClause += "(";

                    if(i!=allFriends.getCurrentPage().size()-1)
                       friendsInClause+= "'" + friend.getObjectId() + "',";
                    else friendsInClause+="'" + friend.getObjectId() + "')";
                }

        }




        if(friendsInClause.length()>0){

            List<FriendsRelate> myFriends = new ArrayList<>();

            BackendlessDataQuery subQuery = new BackendlessDataQuery();

            subQuery.setWhereClause("(UserOne.objectId = '" + invocationUserId + "' and UserTwo.objectId IN " + friendsInClause + ") " +
                    "or (UserTwo.objectId='" + invocationUserId + "' and UserOne.objectId IN " + friendsInClause + ")");

            BackendlessCollection<Map> res = Backendless.Data.of("FriendsRelate").find(subQuery);

            for(int i = 0; i < res.getCurrentPage().size(); i++){
                myFriends.add(new FriendsRelate(res.getCurrentPage().get(i)));
            }

            for(int i = 0; i < myFriends.size(); i++){

                BackendlessUser anotherMy = DataLib.getAnotherFriendFromRelate(myFriends.get(i),invocationUserId);

                for(int j = 0; j < friends.size(); j++){
                    BackendlessUser anotherTheir = DataLib.getAnotherFriendFromRelate(friends.get(j),userId);

                    if(anotherMy.getObjectId().equals(anotherTheir.getObjectId())){
                        friends.get(j).Status = myFriends.get(i).Status;
                        break;
                    }

                }


            }

        }


        return friends;
    };

    public Map<String,Object> readMessages(String poemaId){

        Map<String, Object> message = null;

        QueryOptions queryOptions = new QueryOptions();
        BackendlessDataQuery query = new BackendlessDataQuery();



        List<String> sortBy = new ArrayList<String>();
        sortBy.add( "created DESC" );
        queryOptions.setSortBy(sortBy);
        queryOptions.setPageSize(5);
        query.setWhereClause("poemaId='" + poemaId + "' AND actual=true AND owner != '" + InvocationContext.getUserId() + "'");
        query.setQueryOptions(queryOptions);

        BackendlessCollection<Map> lastMessage = Backendless.Persistence.of("MessageLog").find(query);

        if(lastMessage.getCurrentPage().size()>0){
            message = lastMessage.getCurrentPage().get(0);
            message.put("actual", false);
            Backendless.Persistence.of("MessageLog").save(message);
        }

        return message;
    }

    public Map<String,Object> editMessages(String messageId, String messageText){
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause("messageId = '" + messageId + "'");
        BackendlessCollection<Map> messageCollection = Backendless.Persistence.of("MessageLog").find(query);

        Map<String,Object> message = null;

        if(messageCollection.getCurrentPage().size()>0){
            message = messageCollection.getCurrentPage().get(0);
            message.put("message",messageText);
            Backendless.Persistence.of("MessageLog").save(message);
        } else throw new BackendlessServerException(new BackendlessExceptionMessage(9001, "Error on editMessages"));

        return message;
    }

}
